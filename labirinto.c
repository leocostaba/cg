#include <stdio.h>  
#include <stdlib.h> 
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <GL/glut.h>     
#include <math.h>
#include "load_texture.h"
#define PI 3.14159

//#define __DEBUG


GLfloat posicao[] = {0.0, 500.0, 0.0, 1.0};
GLfloat luz[] = {1.0, 1.0, 1.0, 1.0};
GLfloat ambiente[] = {0.6, 0.6, 0.6, 1.0};

char mapa[1000][1000];
char tilePlayerBegin = 'P', tileWall = '#', tileFloor = '_', tileGoal = 'G';
GLint ang=0, angRotation = 30, vis=0, playerX, playerZ, catetoEmX=10, catetoEmZ=0;

GLuint textureFloor, textureCeiling, textureWall, textureGoal, textureSphere;

int mapLines, mapColumns, k;
int mapTileLength = 10;

char *hedge = "hedge", *brickStone = "brickStone", *ceramic = "ceramic", *grass = "grass", *dark = "dark", *sand = "sand";
char *theme;

int playerSteps = 0, optimalSteps = -1;

int gameOver(){
    GLint xInMap = (playerX + (mapTileLength / 2)) / mapTileLength;
    GLint zInMap = (playerZ + (mapTileLength / 2)) / mapTileLength;

    char tile = mapa[xInMap][zInMap];

    if(tile == tileGoal){
        return 1;
    }
    return 0;
}

int willNotCollide (int currX, int currZ, int xInc, int zInc){
    GLint newX = (currX + xInc + (mapTileLength / 2)) / mapTileLength;
    GLint newZ = (currZ + zInc + (mapTileLength / 2)) / mapTileLength;

    if(newX > mapLines || newX < 0 || newZ > mapColumns || newZ < 0)
        return 0;

    char tile = mapa[newX][newZ];

    if(tile == tileWall)
        return 0;
    else return 1;
}

void printText(char *string, char *actualSteps, char *bfsText, char *bfsSteps){
    glMatrixMode( GL_PROJECTION );
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D( 0, 1280, 0, 1024 );
    glMatrixMode( GL_MODELVIEW );
   
    glPushMatrix();
    glLoadIdentity();
    glRasterPos2i( 20, 20 ); 
    glColor3f(0,0,0);
    while(*string){
        glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24,*string++); 
    }
    glPopMatrix();

    glPushMatrix();
    glLoadIdentity();
    glRasterPos2i( 180, 20 );
    glColor3f(0,0,0);
    while(*actualSteps){
        glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24,*actualSteps++); 
    }
    glPopMatrix();

    glPushMatrix();
    glLoadIdentity();
    glRasterPos2i( 20, 60 );
    glColor3f(0,0,0);
    while(*bfsText){
        glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24,*bfsText++); 
    }
    glPopMatrix();

     glPushMatrix();
    glLoadIdentity();
    glRasterPos2i( 180, 60 );
    glColor3f(0,0,0);
    while(*bfsSteps){
        glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24,*bfsSteps++); 
    }
    glPopMatrix();

    glMatrixMode( GL_PROJECTION );
    glPopMatrix();
    glMatrixMode( GL_MODELVIEW );
}


void parede(bool isGoal){
    glEnable(GL_TEXTURE_2D);
    if(isGoal)
        glBindTexture (GL_TEXTURE_2D,textureGoal);
    else
        glBindTexture (GL_TEXTURE_2D,textureWall);

    glBegin(GL_QUADS); //up
    glNormal3f(0,1,0);
    glTexCoord2f(0,0); glVertex3f(-(mapTileLength/2), (mapTileLength/2), -(mapTileLength/2));
    glTexCoord2f(1,0); glVertex3f( (mapTileLength/2), (mapTileLength/2), -(mapTileLength/2));
    glTexCoord2f(1,1); glVertex3f( (mapTileLength/2), (mapTileLength/2),  (mapTileLength/2));
    glTexCoord2f(0,1); glVertex3f(-(mapTileLength/2), (mapTileLength/2),  (mapTileLength/2));
    glEnd();
    
    glBegin(GL_QUADS); //down
    glNormal3f(0,-1,0);
    glTexCoord2f(0,0); glVertex3f(-(mapTileLength/2), -(mapTileLength/2),-(mapTileLength/2));
    glTexCoord2f(1,0); glVertex3f( (mapTileLength/2), -(mapTileLength/2),-(mapTileLength/2));
    glTexCoord2f(1,1); glVertex3f( (mapTileLength/2), -(mapTileLength/2), (mapTileLength/2));
    glTexCoord2f(0,1); glVertex3f(-(mapTileLength/2), -(mapTileLength/2), (mapTileLength/2));
    glEnd();
    
    glBegin(GL_QUADS); //west
    glNormal3f(0,0,-1);
    glTexCoord2f(0,0); glVertex3f(-(mapTileLength/2), -(mapTileLength/2), -(mapTileLength/2));
    glTexCoord2f(1,0); glVertex3f( (mapTileLength/2), -(mapTileLength/2), -(mapTileLength/2));
    glTexCoord2f(1,1); glVertex3f( (mapTileLength/2),  (mapTileLength/2), -(mapTileLength/2));
    glTexCoord2f(0,1); glVertex3f(-(mapTileLength/2),  (mapTileLength/2), -(mapTileLength/2));
    glEnd();
    
    glBegin(GL_QUADS); //east
    glNormal3f(0,0,1);
    glTexCoord2f(0,0); glVertex3f(-(mapTileLength/2), -(mapTileLength/2), (mapTileLength/2));
    glTexCoord2f(1,0); glVertex3f( (mapTileLength/2), -(mapTileLength/2), (mapTileLength/2));
    glTexCoord2f(1,1); glVertex3f( (mapTileLength/2),  (mapTileLength/2), (mapTileLength/2));
    glTexCoord2f(0,1); glVertex3f(-(mapTileLength/2),  (mapTileLength/2), (mapTileLength/2));
    glEnd();
    
    glBegin(GL_QUADS); //north
    glNormal3f(1,0,0);
    glTexCoord2f(0,0); glVertex3f( (mapTileLength/2), -(mapTileLength/2), -(mapTileLength/2));
    glTexCoord2f(1,0); glVertex3f( (mapTileLength/2), -(mapTileLength/2), (mapTileLength/2));
    glTexCoord2f(1,1); glVertex3f( (mapTileLength/2),  (mapTileLength/2), (mapTileLength/2));
    glTexCoord2f(0,1); glVertex3f( (mapTileLength/2),  (mapTileLength/2), -(mapTileLength/2));
    glEnd();
    
    glBegin(GL_QUADS); //south
    glNormal3f(-1,0,0);
    glTexCoord2f(0,0); glVertex3f(-(mapTileLength/2), -(mapTileLength/2), -(mapTileLength/2));
    glTexCoord2f(1,0); glVertex3f(-(mapTileLength/2), -(mapTileLength/2),  (mapTileLength/2));
    glTexCoord2f(1,1); glVertex3f(-(mapTileLength/2),  (mapTileLength/2),  (mapTileLength/2));
    glTexCoord2f(0,1); glVertex3f(-(mapTileLength/2),  (mapTileLength/2), -(mapTileLength/2));
    glEnd();
    glDisable(GL_TEXTURE_2D);
}

//--------------------------------------------------
void chao(){
    glEnable(GL_TEXTURE_2D);
    glBindTexture (GL_TEXTURE_2D,textureFloor);

    glBegin(GL_QUADS);
    glNormal3f(0,-1,0);
    glTexCoord2f(0,0); glVertex3f(-(mapTileLength/2), -(mapTileLength/2), -(mapTileLength/2));
    glTexCoord2f(1,0); glVertex3f( (mapTileLength/2), -(mapTileLength/2), -(mapTileLength/2));
    glTexCoord2f(1,1); glVertex3f( (mapTileLength/2), -(mapTileLength/2),  (mapTileLength/2));
    glTexCoord2f(0,1); glVertex3f(-(mapTileLength/2), -(mapTileLength/2),  (mapTileLength/2));
    glEnd();

    glDisable(GL_TEXTURE_2D);
}

//--------------------------------------------------------------------------------------
void teto(){
    glEnable(GL_TEXTURE_2D);
    glBindTexture (GL_TEXTURE_2D,textureCeiling);

    glBegin(GL_QUADS);          
    glNormal3f(0,1,0);
    glTexCoord2f(0,0); glVertex3f(-(mapTileLength/2), (mapTileLength/2), -(mapTileLength/2));
    glTexCoord2f(1,0); glVertex3f( (mapTileLength/2), (mapTileLength/2), -(mapTileLength/2));
    glTexCoord2f(1,1); glVertex3f( (mapTileLength/2), (mapTileLength/2),  (mapTileLength/2));
    glTexCoord2f(0,1); glVertex3f(-(mapTileLength/2), (mapTileLength/2),  (mapTileLength/2));
    glEnd();

    glDisable(GL_TEXTURE_2D);
}

//--------------------------------------------------

void camera(){
    int x, z;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);                                      
    glLoadIdentity();
    gluLookAt(5*k-mapTileLength, 5*k+150, 5*k-mapTileLength, 5*k-20, -(5*k+150), 5*k-mapTileLength, 0, 1, 0); 
    for(x=0; x<mapLines; x++){
        for(z=0; z<mapColumns; z++){
            //GLint tile = mapa[x][z];
            char tile = mapa[x][z];

            if(tile == tileWall || tile == tileGoal){
                glPushMatrix();
                glTranslatef(x*mapTileLength, 5, z*mapTileLength);
                parede(tile == tileGoal);
                glPopMatrix();
            }

            if(tile == tileFloor || tile == tilePlayerBegin){
                glPushMatrix();
                glPushMatrix();
                glTranslatef(playerX, 5, playerZ);
                glEnable(GL_TEXTURE_2D);
                glBindTexture (GL_TEXTURE_2D, textureSphere);
                glutSolidSphere(7, 7, 7);
                glDisable(GL_TEXTURE_2D);
                glPopMatrix();
                glTranslatef(x*mapTileLength, 5, z*mapTileLength);
                chao();
                glPopMatrix();
            }
        }
    }            
}

//----------------------------------------------------
void display(){
    int x,z;
    char str[12];
    char str2[12];
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    sprintf(str, "%d", playerSteps);
    sprintf(str2, "%d", optimalSteps);
    gluLookAt(playerX, 7, playerZ, playerX+catetoEmX, 7, playerZ+catetoEmZ, 0, 1, 0);
    for(x=0; x<mapLines; x++){
        for(z=0; z<mapColumns; z++){
            char tile = mapa[x][z];

            if(tile == tileWall || tile == tileGoal){
                glPushMatrix();
                glTranslatef(x*mapTileLength, 5, z*mapTileLength);
                parede(tile == tileGoal);
                glPopMatrix();
            }

            if(tile == tileFloor || tile == tilePlayerBegin){
                glPushMatrix();
                glPushMatrix();
                glTranslatef(playerX, 5, playerZ);
                glPopMatrix();
                glTranslatef(x*mapTileLength, 5, z*mapTileLength);
                chao();
                teto();
                glPopMatrix();
            }
        }
    }
    printText("Passos Efetuados: ", str, "Caminho Mímimo:", str2);   
    if(vis)
        camera();
        glutSwapBuffers();
}

//-----------------------------------------------------------------

void keyboard(unsigned char key, int x, int y){
    switch (key){
        case 27:
        exit(0);
        break;

        case 'm':
        vis = !vis;
        break;
    }
}

//-----------------------------------------------------------------

void mover(int key, int x, int y){
    float rad;
    rad = (float)(PI * ang / 180.0f);
    catetoEmX = cos(rad) * 10;
    catetoEmZ = sin(rad) * 10;
    switch (key){
        case GLUT_KEY_UP:
        if(willNotCollide(playerX, playerZ, catetoEmX, catetoEmZ)){
            playerX = playerX + catetoEmX;
            playerZ = playerZ + catetoEmZ;
            playerSteps++;
        }
        break;

        case GLUT_KEY_DOWN: 
        if(willNotCollide(playerX, playerZ, -catetoEmX, -catetoEmZ)){
            playerX = playerX - catetoEmX;
            playerZ = playerZ - catetoEmZ;
            playerSteps++;
        }
        break;

        case GLUT_KEY_LEFT:
        ang -= angRotation;
        if(ang < 0)
            ang += 360;
        rad = (float)(PI * ang / 180.0f);
        catetoEmX = cos(rad) * 10;
        catetoEmZ = sin(rad) * 10;
        break;

        case GLUT_KEY_RIGHT: 
        ang += angRotation;
        if(ang >= 360)
            ang -= 360;
        rad = (float)(PI * ang / 180.0f);
        catetoEmX = cos(rad) * 10;
        catetoEmZ = sin(rad) * 10;
        break;
    }

    if(gameOver()){
        printf("\033[1;34m");
        printf("\n\nGame Over!\n\n");

        if(playerSteps > optimalSteps){
            printf("\033[0;31m");
            printf("You did not find the optimal path. :(");
            printf("\nSteps Taken = %d/%d", playerSteps, optimalSteps);
        }
        else{
            printf("\033[0;32m");
            printf("You found the optimal path! :D");
        }
        printf("\n\n\n");
        
        sleep(2);
        exit(0);
    }


    #ifdef __DEBUG
    	printf("Player agora em %d %d com angulo %d, catetoEmX %d e catetoEmZ %d\n\n", playerX, playerZ, ang, catetoEmX, catetoEmZ);
    #endif
}
//-----------------------------------------------------------------

typedef struct node {
   int val;
   struct node *next;
} node_t;

void enqueue(node_t **head, int val) {
   node_t *new_node = malloc(sizeof(node_t));
   if (!new_node) return;

   new_node->val = val;
   new_node->next = *head;

   *head = new_node;
}

int dequeue(node_t **head) {
   node_t *current, *prev = NULL;
   int retval = -1;

   if (*head == NULL) return -1;

   current = *head;
   while (current->next != NULL) {
      prev = current;
      current = current->next;
   }

   retval = current->val;
   free(current);

   if (prev)
      prev->next = NULL;
   else
      *head = NULL;

   return retval;
}

void FindOptimalSteps(){
    // node_t *stepsq = NULL;
    // enqueue(&stepsq, 0);
    // int x = dequeue(&stepsq);
    // printf("Queue top = %d\n",x);
    float rad;
    bool bfsVis[mapLines * mapTileLength][mapColumns * mapTileLength];

    node_t *xq = NULL, *zq = NULL, *stepsq = NULL;
    enqueue(&stepsq, 0);
    enqueue(&xq, (int)playerX);
    enqueue(&zq, (int)playerZ);

    #ifdef __DEBUG
        printf("begin bfs at x=%d z=%d\n", playerX, playerZ);
    #endif

    int bfsX, bfsZ, bfsStep;
    while(1 == 1){
        bfsX = dequeue(&xq);
        bfsZ = dequeue(&zq);
        bfsStep = dequeue(&stepsq);

        GLint xInMap = (bfsX + (mapTileLength / 2)) / mapTileLength;
        GLint zInMap = (bfsZ + (mapTileLength / 2)) / mapTileLength;

        char tile = mapa[xInMap][zInMap];

        #ifdef __DEBUG
            printf("bfs x=%d z=%d s=%d xm=%d zm=%d tile=%c\n", bfsX, bfsZ, bfsStep, xInMap, zInMap, tile);
        #endif

        if(tile == tileGoal){
            optimalSteps = bfsStep;
            break;
        }

        int bfsCatetoEmX, bfsCatetoEmZ;
        for(int bfsAng = 0; bfsAng < 360; bfsAng += angRotation){
            rad = (float)(PI * bfsAng / 180.0f);
            bfsCatetoEmX = cos(rad) * 10;
            bfsCatetoEmZ = sin(rad) * 10;
            if(
                bfsX + bfsCatetoEmX >= 0 && bfsZ + bfsCatetoEmZ &&
                !bfsVis[bfsX + bfsCatetoEmX][bfsZ + bfsCatetoEmZ] &&
                willNotCollide(bfsX, bfsZ, bfsCatetoEmX, bfsCatetoEmZ)){
                enqueue(&xq, bfsX + bfsCatetoEmX);
                enqueue(&zq, bfsZ + bfsCatetoEmZ);
                enqueue(&stepsq, bfsStep+1);
                bfsVis[bfsX + bfsCatetoEmX][bfsZ + bfsCatetoEmZ] = true;
            }
        }
    }

    #ifdef __DEBUG
        printf("saida mais proxima em x=%d z=%d s=%d\n", bfsX, bfsZ, bfsStep);
    #endif
/*
    */
}

//-----------------------------------------------------------------

void Inicializa(){
    if(strcmp(theme, brickStone) == 0){
        textureWall = loadBMP("textures/rdbrick.bmp");
        textureFloor = loadBMP("textures/rock162.bmp");
        textureCeiling = loadBMP("textures/rock162.bmp");
        textureSphere = loadBMP("textures/esfera.bmp");
        textureGoal = loadBMP("textures/harem.bmp");
    }
    if(strcmp(theme, hedge) == 0){
        textureWall = loadBMP("textures/hedgeWall.bmp");
        textureFloor = loadBMP("textures/grass.bmp");
        textureCeiling = loadBMP("textures/clouds.bmp");
        textureSphere = loadBMP("textures/esfera.bmp");
        textureGoal = loadBMP("textures/harem.bmp");
    }
    if(strcmp(theme, ceramic) == 0){
        textureWall = loadBMP("textures/ceramicWall.bmp");
        textureFloor = loadBMP("textures/floor.bmp");
        textureCeiling = loadBMP("textures/clouds.bmp");
        textureSphere = loadBMP("textures/esfera.bmp");
        textureGoal = loadBMP("textures/harem.bmp");
    }
    if(strcmp(theme, grass) == 0){
        textureWall = loadBMP("textures/wall2.bmp");
        textureFloor = loadBMP("textures/grass2.bmp");
        textureCeiling = loadBMP("textures/clouds.bmp");
        textureSphere = loadBMP("textures/esfera.bmp");
        textureGoal = loadBMP("textures/harem.bmp");
    }
    if(strcmp(theme, dark) == 0){
        textureWall = loadBMP("textures/ceramicWall.bmp");
        textureFloor = loadBMP("textures/floor.bmp");
        textureCeiling = loadBMP("textures/skydark.bmp");
        textureSphere = loadBMP("textures/esfera.bmp");
        textureGoal = loadBMP("textures/harem.bmp");
    }
    if(strcmp(theme, sand) == 0){
        textureWall = loadBMP("textures/concret.bmp");
        textureFloor = loadBMP("textures/sand.bmp");
        textureCeiling = loadBMP("textures/clouds.bmp");
        textureSphere = loadBMP("textures/esfera.bmp");
        textureGoal = loadBMP("textures/harem.bmp");
    }

    playerX = 0;
    playerZ = 0;
    int bestRadius = 1;
    bool playerStartNotSpecified = true;
    for(int x=0; x<mapLines && playerStartNotSpecified; x++){
        for(int z=0; z<mapColumns && playerStartNotSpecified; z++){
            if(mapa[x][z] == tilePlayerBegin){
                playerX = x * mapTileLength;
                playerZ = z * mapTileLength;
                playerStartNotSpecified = false;
                break;
            }

            // just in case the map does not specify where the player begins,
            // we put him in the middle of the largest empty space
            bool mightBeLargest = true;
            while(mightBeLargest){
                int biggerRadius = bestRadius+1;
                if(mapa[x][z] == tileFloor &&
                    x >= biggerRadius && x <= mapLines-1-biggerRadius &&
                    z >= biggerRadius && z <= mapColumns-1-biggerRadius){

                    for(int i=x-biggerRadius; i<=x+biggerRadius; i++)
                        for(int j=z-biggerRadius; j<=z+biggerRadius; j++)
                            if(sqrt(pow(x-i,2) + pow(z-j,2)) <= biggerRadius)
                                if(mapa[i][j] != tileFloor)
                                    mightBeLargest = false;

                    if(mightBeLargest){
                        bestRadius = biggerRadius;
                        playerX = x * mapTileLength;
                        playerZ = z * mapTileLength;
                        #ifdef __DEBUG
                            printf("start overtaken by (%d,%d) with radius %d\n", x, z, bestRadius);
                        #endif
                    }
                }
                else{
                    mightBeLargest = false;
                }
            }
        }
    }

    glShadeModel(GL_SMOOTH);
    glClearColor(0.1, 0.6, 0.1, 1.0);
    glLightfv(GL_LIGHT0, GL_POSITION, posicao);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, luz);
    glLightfv(GL_LIGHT0, GL_SPECULAR, luz);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambiente);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(90,1,0.1,3000);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glEnable(GL_DEPTH_TEST);
}

//--------------------------------------------------------------

void main(int argc, char **argv){
	#ifdef __DEBUG
		printf("DEBUG LIGADO!!!\n");
    #endif

    glutInit(&argc, argv);

    theme = argv[1];

    for(int x=0; x<1000; x++)
        for(int z=0; z<1000; z++)
            mapa[x][z] = tileWall;

    int i,j;
    scanf("%d %d", &mapLines, &mapColumns);
    if(mapLines>1000 || mapColumns>1000){
        printf("Tamanho invalido!\n");
        return;
    }
    else{
        bool hasExit = false;
        for(i=0;i<mapLines;i++){
            for(j=0;j<mapColumns;j++){
                char input;
                scanf("%c", &input);
                if(input == ' ' || input == '\n' || input == 15/*CR*/)
                    j--;
                else
                    mapa[mapLines-i-1][j] = input;

                if(input == tileGoal) hasExit = true;
            }
        }

        k = (mapLines >= mapColumns)
            ? mapLines
            : mapColumns;

        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
        glutCreateWindow("Labirinto 3D");
        glutFullScreen();  
        Inicializa();
        if(hasExit)
            FindOptimalSteps();
        glutDisplayFunc(display);
        glutKeyboardFunc(keyboard);    
        glutSpecialFunc(mover);
        glutIdleFunc(glutPostRedisplay);
        glutMainLoop();
    }
}