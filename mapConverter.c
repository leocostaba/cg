//This program loads 3-channel BMP
#include <GL/glut.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

//#define __DEBUG

//gcc -o executable mapConverter.c -lglut -lGLU -lGL -lm && ./executable maps/circulo.bmp

// bitmap header structure, has to be packed to avoid compiler padding
#pragma pack(1)
typedef struct BITMAPFILEHEADER {
  char magic[2];       // "BM" 0x424d - char[2] to avoid endian problems
  uint32_t filesize;   // size of the bitmap file (data + headers)
  uint16_t reserved1;  // must be 0
  uint16_t reserved2;  // must be 0
  uint32_t dataoffset; // when does the data start
} BITMAPFILEHEADER;

typedef struct BITMAPINFOHEADER {
  uint32_t headersize;      // size of this header
  int32_t width;            // width of the bmp
  int32_t height;           // height of the bmp
  uint16_t colorplanes;     // must be 1
  uint16_t bitdepth;        // bits per pixel
  uint32_t compression;     // 0 = uncompressed
  uint32_t imagesize;       // can be 0 if bmp is uncompressed
  int32_t hresolution;      // print resolution
  int32_t vresolution;      // print resolution
  uint32_t palettecolors;   // can be 0 if uncompressed
  uint32_t importantcolors; // number of important colors, 0 = all are important
} BITMAPINFOHEADER;

typedef struct BITMAPFULLHEADER {
  BITMAPFILEHEADER fileinfo;
  BITMAPINFOHEADER bmpinfo;
} BITMAPFULLHEADER;
#pragma pack(0)

// BMP HEADER
BITMAPFULLHEADER header;

// image data
unsigned char *data;        // loaded image

// dynamic programming to improve performance

int loadBMP(const char *imagepath) {
  // Open the file
    FILE *file = fopen(imagepath, "rb");
    if (!file) {
        printf("Image could not be opened\n");
        return 0;
    }

    // Read header
    if (fread(&header, 1, sizeof(BITMAPFULLHEADER), file) !=
      54) { // If not 54 bytes read : problem
        printf("Not a correct BMP file - Wrong header size\n");
    return 1;
}

if (header.fileinfo.magic[0] != 'B' || header.fileinfo.magic[1] != 'M') {
    printf("Not a correct BMP file - Wrong magic number\n");
    return 1;
}

    // Read ints from the header
unsigned long dataPos = header.fileinfo.dataoffset;
unsigned long imageSize = header.bmpinfo.imagesize;
long width = header.bmpinfo.width;
long height = header.bmpinfo.height;

    // Some BMP files are misformatted, guess missing information
if (imageSize == 0) {
    imageSize = width * height * 3;
}
if (dataPos == 0) {
        dataPos = 54; // The BMP header is usually done that way
    }

    // Allocate buffer
    data = (unsigned char *)malloc(imageSize);

    // Read the actual data from the file into the buffer
    fseek(file, dataPos, SEEK_SET);
    if (!fread(data, 1, imageSize, file)) {
        printf("Couldn't read BMP Data. Giving up.\n");
        return 1;
    }

    int grayScales[height][width];
    int limiarUp = 240, limiarDown = 15;
    int B,G,R, biggestGrayScale = 0, lowestGrayScale = 255;
    for(int i = 0; i < height; i++){
        for(int j = width-1; j >= 0; j--){
            int index = 3*(i*width+j);
            B = data[index];
            G = data[index+1];
            R = data[index+2];
            int gs = (R+G+B)/3;

            if((G >= limiarUp && R <= limiarDown && B <= limiarDown ||
              G <= limiarDown && R <= limiarDown && B >= limiarUp))
                grayScales[i][j] = 255;
            else if(i == 0 || i == height-1 || j == 0 || j == width-1)
                grayScales[i][j] = 0;
            else{
                grayScales[i][j] = gs;
                if(gs > biggestGrayScale) biggestGrayScale = gs;
                if(gs < lowestGrayScale) lowestGrayScale = gs;
            }
        }
    }

    #ifdef __DEBUG
    printf("biggestGrayScale = %d e lowestGrayScale = %d\n", biggestGrayScale, lowestGrayScale);
    #endif

    printf("%ld %ld\n", height, width);
    for(int i = 0; i < height; i++){
        for(int j = width-1; j >= 0; j--){
            int index = 3*(i*width+j);
            B = data[index];
            G = data[index+1];
            R = data[index+2];

            if(i == 0 || i == height-1 || j == 0 || j == width-1){
                printf("#"); // BORDER
            }
            else{
              if(G >= limiarUp && R <= limiarDown && B <= limiarDown)
                    printf("P"); // PLAYER START
              else if(G <= limiarDown && R <= limiarDown && B >= limiarUp)
                    printf("G"); // PLAYER GOAL
              else{
                int gs = (R+G+B)/3;
                if(abs(gs-biggestGrayScale) > abs(gs-lowestGrayScale)){
                    printf("#");
                }
                else{
                    int sumOf3by3 = 0;
                    for(int line=i-1; line<=i+1; line++){
                        for(int col=j-1; col<=j+1; col++){
                            sumOf3by3 +=
                                abs(grayScales[line][col]-biggestGrayScale) > abs(grayScales[line][col]-lowestGrayScale)
                                ? 0
                                : 255;
                        }
                    }
                    gs = sumOf3by3 / 9;
                    #ifdef __DEBUG
                    printf("(%d,%d) tem soma %d e media %d\n", i, j, sumOf3by3, gs);
                    #endif
                    if(abs(gs-biggestGrayScale) > abs(gs-lowestGrayScale))
                        printf("#");
                    else
                        printf("_");
                }
            }
        }
    }
    printf("\n");
}

fclose(file);
return 0;
}

int main(int argc, char **argv) {
  loadBMP(argv[1]);
  return 0;
}
